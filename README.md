# PHP 5.6.x 書き方サンプル

## php.ini

php 設定ファイルの書き方とか、最初にやっておく事。  
チューニング周りは状況によって変わるので今調査はしない。

### コメントアウト

`;` を使う事。  
`#` は使用しない。5.3 から警告対象に。7.0 からは動作しない。

### 日本語設定 (utf-8)

マルチバイト文字列を設定する場合、mbstring の設定を変更する。

参考URL

- http://www.tokumaru.org/d/20100927.html

```
; マルチバイト日本語設定 (utf-8)
[mbstring]
mbstring.language = Japanese
mbstring.internal_encoding = UTF-8
mbstring.http_input = UTF-8
mbstring.http_output = pass
mbstring.encoding_translation = Off
mbstring.detect_order = UTF-8,SJIS,EUC-JP,JIS,ASCII
mbstring.substitute_character = "?"
mbstring.func_overload = 0
mbstring.strict_detection = Off
mbstring.http_output_conv_mimetypes = "^(text/|application/(xhtml\+xml|json))"
```

### セキュリティ

#### 不要モジュールの削除

緊急性は無い為あとで調査。

`# php -m` で現ロードモジュールは確認できる。

#### .ini ファイル

参考 URL

- https://www.cyberciti.biz/tips/php-security-best-practices-tutorial.html


```
; 関数制限 (不要な関数、使ってはいけない関数を制限する)
disable_functions = phpinfo,exec,passthru,shell_exec,system,proc_open,popen,curl_exec,curl_multi_exec,parse_ini_file,show_source

; php 情報をヘッダに出さない
expose_php = Off

; # PHP エラーログ設定.
; エラーログの有効化
log_errors = On
; エラーログの出力先 (filepath or syslog)
error_log = /var/log/php_error.log
; エラー出力の表示の有無 (debug 用の為、本番では Off にする)
display_errors = Off

; # ファイルアップロードを拒否する.
; file アップロード
file_uploads = Off
; アップロードを許可する場合 (ファイルサイズに制限をかけること)
; file_uploads = On
; upload_max_filesize = 1M

; # リモートコード実行をオフにする.
; リモートリソースの取得の許可 (On にすると code injection が発生する危険性が出る)
allow_url_fopen = Off
allow_url_include = Off

; # SQL セーフモード.
; 有効にする事で mysql_connect, mysql_pconnect の使用を制限する (どちらも非推奨 DB 接続関数).
sql.safe_mode = On

; # POST size に制限をかける.
; ソーシャルゲームの場合では大きな POST があり得るのでこれくらいでも良いかも? fileupload にも影響がでる.
; (制限自体は必要 MB,GB サイズで攻撃される可能性があるので)
post_max_size = 10K

; # リソースコントロール (DOS 対策).
; チューニング対象 (処理内容で決める必要あり)
; php スクリプトの最大実行時間 (秒)
max_execution_time = 30
; リクエストパラメータの最大実行時間 (秒)
max_input_time = 30
; memory_limit = 40M

; # PHP のファイルアクセス制限.
; 指定ディレクトリ以下へのアクセスを制限する
open_basedir = "/var/www/html/"
```
