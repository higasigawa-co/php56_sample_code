<?php
# PHP の省略表記タグはマニュアルによると非推奨
# 理由は、使用可能かどうかが .ini の設定に依存するため

# ステートメントの区切りにはセミコロンを忘れず付ける
echo "ハロー<br/>";

/* 
   複数行コメント
   C 言語と一緒
 */
# スクリプト型コメント
// C++型 のコメントも可能なのでコーディング規約に従って使う

# 変数には $ を prefix として付ける
# boolean 大文字、小文字に依存しない (True, true も可)
$a_bool = TRUE;

# string "" or ''
$a_str = "foo";

# integer
$an_int = 12;

# gettype で型を取得 (人間用)
# 型チェックをする用途では is_* (type) を使う
echo gettype($a_bool), '<br/>';
echo is_bool($a_bool), '<br/>';

# true => 1, false => '' という謎仕様
echo is_int($a_bool), '<br/>';
var_dump(is_string($a_bool)); echo '<br/>';
# (type) value でキャスト
echo is_bool((bool)'false'), '<br/>'; # => 1

# PHP の整数最大値
var_dump(PHP_INT_MAX); echo '<br/>'; # => 9223372036854775807 (符号付き 64bit size 2^63)
# PHP で整数をオーバーフローさせると float 型に変わってしまう...
var_dump(PHP_INT_MAX+1); echo '<br/>'; # => 9223372036854775807
# 因みに計算中に一回でもオーバーフローすると float になって整数には勝手には戻らない
var_dump(PHP_INT_MAX+1-10000000000000000); echo '<br/>';
# 無理矢理 int にキャストすると符号が逆転する (これ自体は納得行くけど上記があるので納得がいかない...)
var_dump((int)(PHP_INT_MAX+1)); echo '<br/>'; # => -9223372036854775808

# 割り算で小数になる場合は float に変換される
echo 2/2, '<br/>';
echo 1/2, '<br/>';

# 丸めたい時は round か (int) でキャスト
# ただし普通に使うと結果が異なる為ちゃんと round で丸めた方が良い (option で丸め方を指定出来る)
# 切り捨ては floor 返り値は float
# 切り上げは ceil  返り値は float
echo round(1/2), '<br/>'; # => 1
echo (int)(1/2), '<br/>'; # => 0

# NaN (Not a Number)
echo acos(1.01), '<br/>';
# NaN チェック
# is_nan を使わない限り同条件、自分自身との比較も false になる
var_dump(acos(1.01) == acos(1.01)); echo '<br/>';
var_dump(is_nan(acos(1.01))); echo '<br/>';

# 文字列 '' と "" の違い
# '' の場合はエスケープシーケンス、変数展開等が無効な文字列になる
# 展開されない
$var = 'ひがしがわ';
echo '名前: $var<br/>';
# '' を使う場合は結合させる
echo '名前: ' . $var, '<br/>';

# "" の場合は展開される
echo "名前: $var<br/>";
# 変数展開された名前の直後に s を置きたい
echo "名前: $vars<br/>";
# 変数のパース範囲を明示的にする
echo "名前: {$var}s<br/>";

# 配列定義
# 配列といいながら実態は連想配列で、途中から key => value の指定にしても通る
$array = array("bar", "baz", 0, "hoge" => "fuga");
var_dump($array); echo '<br/>';

function getArray() {
  return array(1, 2, 3);
}
# 5.4 以降は関数の戻り値 (リファレンス) に対して直接値の取得をかけられる
var_dump(getArray()[0]);
# 5.3 以前はこう
$tmp = getArray(); echo '<br/>';
var_dump($tmp[0]); echo '<br/>';

# array[] に代入すると、その配列内の key の一番大きい値 +1 を key に自動で選択する
$array = array(5 => 1, 12 => 2);
$array[] = 56;
var_dump($array); echo '<br/>';
# key に文字を入れても数値だけを見て key 最大値 +1 を選んでくる...
$array = array(5 => 1, 12 => 2, 'tmp' => 3);
$array[] = 56;
var_dump($array); echo '<br/>';

# 要素の削除
unset($array[13]);
var_dump($array); echo '<br/>';
# 配列そのものの削除
unset($array);
var_dump($array); echo '<br/>';


# オブジェクトの扱い
class foo
{
  function do_foo()
  {
    echo 'foo を実行します<br/>';
  }

  static function do_bar()
  {
    echo 'foo->bar を実行します<br/>';
  }
}

# object 生成は new 命令を使う
$bar = new foo;
# method 実行は instance->method
$bar->do_foo();
var_dump($bar); echo '<br/>';

# コールバック
print_r( call_user_func('getArray') ); echo '<br/>';
# オブジェクトメソッドのコール
print_r( call_user_func(array($bar, 'do_foo')) );
# 静的クラスメソッドのコール
print_r( call_user_func(array('foo', 'do_bar')) );
# 静的クラスメソッドのコール (直感的な方)
print_r( call_user_func('foo::do_bar') );

# クロージャ
# function($tmp) => 無名関数
# use () を無名関数に付ける事で変数を引き継げる
# さらに & を変数に付ける事でクロージャ内に参照を渡して変数を共有する事が出来る
# 呼び出し毎に、関数内に前回の実行結果が保持できる => クロージャ
function closure_counter() {
  $sum = 0;
  return function($tmp) use (&$sum) {
    $sum += $tmp;
    return $sum;
  };
}

$closure = closure_counter();

echo $closure(1), '<br/>';
echo $closure(1), '<br/>';

# ファイル全体が PHP 純粋なスクリプトの場合、PHP の閉じタグは省略する事が推奨されている
# 理由は閉じタグ以降に余分な改行がある事で意図しない動作を起こす可能性がある為
